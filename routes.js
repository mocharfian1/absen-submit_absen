'use strict';

module.exports = function(app) {
    var controller = require('./controller');

    app.route('/')
        .get(controller.index);

    app.route('/submitAbsenMasuk')
        .post(controller.submitAbsenMasuk);

    app.route('/submitAbsenPulang')
        .post(controller.submitAbsenPulang);
};