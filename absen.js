var express = require('express'),
    app = express(),
    port = process.env.PORT || 50003,
    bodyParser = require('body-parser'),
    controller = require('./controller');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./routes');
routes(app);

app.listen(port);
console.log('Absen BKPSDM by Moch Arfian Ardiansyah: ' + port);