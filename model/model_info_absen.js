'use strict';

var response = require('../res');
var connection = require('../conn');
var MDResult = require('./model_result');
const request = require('request');
var moment = require('moment');

exports.getSessionInfo = function (token=null,callback=null){
    return new Promise((resolve,reject) =>{
        var query = `SELECT * from maps_user_session where token='`+token+`'`;

        connection.query(query, (err,result)=>{
            if(err){
                return reject("Error get Session");
            }else{
                if(result.length > 0){
                    return resolve(result[0]);
                }else {
                    return reject("Session Not Found");
                }
            }
        });
    });
}

var resultInfoPegawai;

exports.getInfoAndSubmitAbsenMasuk = function (nip=null,body=null,token=null){
    return new Promise((resolve,reject) =>{
        var dayNumber = new Date().getDay()+1;
        var query = `   SELECT 
                            p.nama,
                            p.nip,
                            po.id as id_pola,
                            po.nama_pola,
                            pj.open_absen,
                            pj.close_absen,
                            pj.jam_masuk,
                            pj.jam_pulang, 
                            p.wfh_on,
                            p.wfo_on
                        FROM 
                            maps_pegawai p
                            JOIN maps_pola po ON p.pola=po.id
                            left JOIN maps_pola_jadwal pj ON po.id=pj.id_pola AND pj.hari_number=${dayNumber}
                        WHERE 
                            p.nip='${nip}'`;

        connection.query(query, (err,result)=>{
            if(err){
                return reject("[Error] Prepare Absen");
            }else{
                resultInfoPegawai = result[0];
                if(result.length > 0){
                    var now = new Date();
                    var curTime = moment().format('HH:mm:ss');

                    if(resultInfoPegawai.open_absen === null){
                        return reject("Tidak ada absen untuk hari ini");
                    }else{
                        console.log("--> prepare to Logic Absen Masuk");
                        if(curTime > resultInfoPegawai.open_absen && curTime < resultInfoPegawai.close_absen){

                            if(curTime > resultInfoPegawai.jam_pulang && curTime < resultInfoPegawai.close_absen){
                                console.log("--> [Error] 602");
                                return reject("Gagal melakukan Absen Masuk. Diluar jam Absen Masuk.");
                            }
                            else{

                                checkAbsenMasuk(nip).then((responseCheckAbsen)=>{
                                    if(responseCheckAbsen.length>0){
                                        return reject("Data absen hari ini sudah ada.");
                                    }else{
                                        var hari = ["MINGGU","SENIN","SELASA","RABU","KAMIS","JUMAT","SABTU","MINGGU"];
                                        var insertQuery = "INSERT INTO maps_absen (`token`,`token_absen`, `absen_masuk`, `absen_pulang`,`type_absen`, `hari`, `pola_absen`, `pola_toleransi`, `latitude`, `longitude`, `accuracy`, `nip`, `nama`, `image_absen`,`device`) " +
                                            "VALUES ('"+ token +"','"+body.token_absen+"', '"+moment().format('YYYY-MM-DD HH:mm:ss')+"' , NULL,'"+body.type_absen+"', '"+ hari[new Date().getDay()] +"', '"+resultInfoPegawai.id_pola+"', NULL, '"+body.latitude+"', '"+body.longitude+"', '"+body.accuracy+"', '"+nip+"', '"+resultInfoPegawai.nama+"', '','"+body.device+"');\n";
                                        connection.query(insertQuery, function (err, result) {
                                            if (err){
                                                console.log("--> [Error] 603");
                                                return reject("Gagal menambahkan data. Error Server.");
                                            }else{
                                                console.log("--> [Info] Success");
                                                return resolve({
                                                    success:true,
                                                    message:"Berhasil melakukan absen masuk.",
                                                    token_absen:body.token_absen,
                                                    username:resultInfoPegawai.nip
                                                });
                                            }

                                        });
                                    }
                                }).catch((e)=>{
                                    return reject(e);
                                });
                            }

                        }else{
                            return reject("Diluar waktu absen masuk");
                        }
                    }
                }else {
                    return reject("Data not found");
                }

            }
        });
    });
}

function checkAbsenMasuk(nip){
    return new Promise((resolve, reject)=>{
        let queryCheckAbsenMasuk = `select * from maps_absen where date(insert_date) = date('`+moment().format('YYYY-MM-DD')+`') and nip='`+nip+`'`;
        connection.query(queryCheckAbsenMasuk,(err,res)=>{
            if(err){
                return reject("Gagal mendapatkan data. Error Server.");
            }else{
                return resolve(res);
            }
        })
    });

}

exports.getInfoAndSubmitAbsenPulang = function (nip=null,body=null,token=null){
    return new Promise((resolve,reject) =>{
        var dayNumber = new Date().getDay()+1;
        var query = `   SELECT 
                            p.nama,
                            p.nip,
                            po.id as id_pola,
                            po.nama_pola,
                            pj.open_absen,
                            pj.close_absen,
                            pj.jam_masuk,
                            pj.jam_pulang, 
                            p.wfh_on,
                            p.wfo_on
                        FROM 
                            maps_pegawai p
                            JOIN maps_pola po ON p.pola=po.id
                            left JOIN maps_pola_jadwal pj ON po.id=pj.id_pola AND pj.hari_number=${dayNumber}
                        WHERE 
                            p.nip='${nip}'`;
        console.log(query);

        connection.query(query, (err,result)=>{
            console.log("--> to Logic Update Pulang");
            if(err){
                return reject("[Error] Prepare Absen");
            }
            else{
                resultInfoPegawai = result[0];
                if(result.length > 0){
                    var now = new Date();
                    var curTime = moment().format('hh:mm:ss');

                    if(resultInfoPegawai.open_absen === null){
                        return reject("Tidak ada absen untuk hari ini");
                    }else{
                        if(curTime > resultInfoPegawai.open_absen && curTime < resultInfoPegawai.close_absen){

                            if(curTime > resultInfoPegawai.jam_pulang && curTime < resultInfoPegawai.close_absen){
                                return reject("Gagal melakukan Absen Pulang. Diluar jam Absen Pulang.");
                            }else{
                                this.getAbsenCurrentDate(nip).then((resAbsenPulang)=>{
                                    if(resAbsenPulang){
                                        if (resAbsenPulang.absen_pulang === null){
                                            var updateAbsenPulang = "UPDATE maps_absen set absen_pulang='"+moment().format('YYYY-MM-DD HH:mm:ss')+"' WHERE nip='"+nip+"'";
                                            connection.query(updateAbsenPulang,()=>{
                                                if (err){
                                                    return reject("Gagal melakukan Absen Pulang");
                                                }else{
                                                    return resolve({
                                                        success:true,
                                                        message:"Berhasil melakukan Absen Pulang.",
                                                        username:resultInfoPegawai.nip
                                                    });
                                                }
                                            });
                                        }else{
                                            return reject("[ERROR] Anda sudah melakukan absen Pulang.");
                                        }

                                    }else{
                                        return reject("[ERROR] Data tidak ditemukan.");
                                    }
                                });
                                // var updateAbsenPulang = "UPDATE maps_absen set absen_pulang='"+moment().format('YYYY-MM-DD HH:mm:ss')+"' WHERE nip='"+nip+"'";
                                // connection.query(updateAbsenPulang,()=>{
                                //     if (err){
                                //         return reject("Gagal melakukan Absen Pulang");
                                //     }else{
                                //         return resolve({
                                //             success:true,
                                //             message:"Berhasil melakukan Absen Pulang.",
                                //             username:resultInfoPegawai.nip
                                //         });
                                //     }
                                // });

                            }

                        }else{
                            return reject("Diluar waktu absen pulang.");
                        }
                    }
                }
                else {
                    return reject("Data not found");
                }

            }
        });
    });
}

exports.getAbsenCurrentDate = function (nip=null){
    return new Promise((resolve, reject)=>{
        var query = "SELECT * FROM maps_absen WHERE DATE(insert_date)='"+moment().format('YYYY-MM-DD')+"' AND nip='"+nip+"'";
        connection.query(query,(err,result)=>{
            if(err){
                return reject("Error Connection");
            }else{
                if(result.length>0){
                    return resolve(result[0]);
                }else{
                    return reject("Data not found.");
                }

            }
        });
    });

};

function getCurrentAbsen(token_absen=null){
    console.log(token_absen);
    return new Promise((resolve,reject)=>{
        var queryCurrentAbsen = "select * from maps_absen where token_absen='"+token_absen+"'";
        connection.query(queryCurrentAbsen, (err,result)=>{
            if(err){
                return reject("Gagal mendapatkan data.");
            }else{
                if(result.length>0){
                    return resolve(true);
                }else{
                    return reject("Data tidak ditemukan");
                }
            }
        });
    });
}