'use strict';

var response = require('./res');
var MDInfoAbsen = require('./model/model_info_absen');
var MDResult = require('./model/model_result');
var MDGetCheat = require('./model/model_get_cheat');
var connection = require('./conn');

exports.index = function(req, res) {
    response.send("",false,"Bad Credentials",{}, res);
};

//////////////////////////////////////////////  GET PREPARE MAPS LOCATION ///////////////////////////////////////

var global_username;
exports.submitAbsenMasuk = function (req, res){
    const { headers } = req;
    if(headers['x-token-access'].length > 0){
        var token = headers['x-token-access'];
        console.log(token);
        MDInfoAbsen.getSessionInfo(token).then((infoSession)=>{
            if(infoSession) {
                console.log("--> getSession");
                global_username = infoSession.nip;
                MDInfoAbsen.getInfoAndSubmitAbsenMasuk(global_username,req.body,token).then((result) => {
                    if(result.success){
                        response.send(result.username,true,result.message,{},res);
                    }else{
                        response.send(result.username,false,result.message,{},res);
                    }
                }).catch((e)=>{
                    response.send(global_username,false,e,{},res);
                });
            }else{
                response.send(global_username,false,"Bad Credentials.",{},res);
            }
        }).catch((e)=>{
            response.send("",false,e,{},res);
        });


    }else{
        response.send("",false,"Bad Credentials.",{},res);
    }
}

exports.submitAbsenPulang = function (req, res){
    const { headers } = req;
    if(headers['x-token-access'].length > 0){
        var token = headers['x-token-access'];
        console.log(token);
        MDInfoAbsen.getSessionInfo(token).then((infoSession)=>{
            if(infoSession) {
                console.log("--> getSession");
                global_username = infoSession.nip;
                MDInfoAbsen.getInfoAndSubmitAbsenPulang(global_username,req.body,token).then((result) => {
                    if(result.success){
                        response.send(result.username,true,result.message,{},res);
                    }else{
                        response.send(result.username,false,result.message,{},res);
                    }
                }).catch((e)=>{
                    response.send(global_username,false,e,{},res);
                });
            }else{
                response.send(global_username,false,"Bad Credentials.",{},res);
            }
        }).catch((e)=>{
            response.send("",false,e,{},res);
        });


    }else{
        response.send("",false,"Bad Credentials.",{},res);
    }
}